package net.bnac.cbi.xnat.plugin;

import org.nrg.xdat.bean.*;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.framework.annotations.XnatDataModel;
import org.springframework.context.annotation.Bean;

@XnatPlugin(value = "clinicalDemographicDataTypes", name = "Clinical and Demographic Data Types",
        description = "Clinical and demographic data types for XNAT",
        dataModels = {@XnatDataModel(value = CddDiseasegroupBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Disease group",
                                    plural = "Disease groups",
                                    code = "DG"),
                @XnatDataModel(value = CddDiseasedurationBean.SCHEMA_ELEMENT_NAME,
                        singular = "Disease duration",
                        plural = "Disease durations",
                        code = "DD"),
                @XnatDataModel(value = CddEdssBean.SCHEMA_ELEMENT_NAME,
                        singular = "EDSS",
                        plural = "EDSS",
                        code = "EDSS")})
public class ClinicalDemographicDataTypes {
    @Bean
    public String clinicalDemographicDataTypesMessage() {
        return "Is this thing on?";
    }
}
